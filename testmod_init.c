#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include "testmod_math.h"

static int __init testmod_init(void)
{
	double a;
	pr_info("Hello from Test Mod\n");
	//a = domath();

	return 0;
}

static void __exit testmod_exit(void)
{
	pr_info("Goodbye from Test Mode\n");
}

module_init(testmod_init);
module_exit(testmod_exit);

MODULE_LICENSE("GPL");
