#pragma GCC target("sse,mmx,avx2,bmi2,avx,sse3,fma")
#define _MM_MALLOC_H_INCLUDED
#include "immintrin.h"
#include "testmod_math.h"

__attribute__((target("avx,sse,sse3")))
double domath2(void) 
{

	int x;
	double ret[2];
	__m128d temp1 = _mm_set1_pd(1.0);
	__m128d temp2 = _mm_set1_pd(2.0);	
	__m128d temp4 = _mm_set1_pd(3.0);
		
	__m128d temp3 = _mm_fmaddsub_pd( temp1, temp2, temp4);
	for(x=0; x<10; ++x)
	{
		_mm_store_pd(ret, temp3);
	}
	return ret[0];
}

void kernel_fpu_begin(void);   // stub declarations for a stand-alone test file
void kernel_fpu_end(void);

double domath(void)
{
	kernel_fpu_begin();
	domath2();
	kernel_fpu_end();
}
